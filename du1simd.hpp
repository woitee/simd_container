#include <iterator>
#include <memory>

template< typename T, typename S>
class simd_vector_simd_iterator;

template< typename T, typename S>
class simd_vector_iterator {
	T* ptr;
public:
	typedef std::random_access_iterator_tag iterator_category;
	typedef T value_type;
	typedef std::size_t size_type; //not necessary
	typedef std::ptrdiff_t difference_type;
	typedef T* pointer;
	typedef T& reference;

	static const size_t K = sizeof(S)/sizeof(T);
public:
	simd_vector_iterator() : ptr(nullptr) { }
	simd_vector_iterator(T* p) : ptr(p) { }
	simd_vector_iterator(T* p, std::size_t offset) : ptr(p + offset) { }
	//copy constructors, assignment operators, and their move variants default

	// ACCESS OPERATORS
	reference operator* () {
		return *ptr;
	}
	reference operator[] (difference_type n) {
		return *(ptr + n);
	}
	pointer operator-> () {
		return ptr;
	}

	// LOGICAL OPERATORS
	bool operator== (const simd_vector_iterator& oth) const {
		return ptr == oth.ptr;
	}
	bool operator!= (const simd_vector_iterator& oth) const {
		return ptr != oth.ptr;
	}
	bool operator< (const simd_vector_iterator& oth) const {
		return ptr < oth.ptr;
	}
	bool operator<= (const simd_vector_iterator& oth) const {
		return ptr <= oth.ptr;
	}
	bool operator> (const simd_vector_iterator& oth) const {
		return ptr > oth.ptr;
	}
	bool operator>= (const simd_vector_iterator& oth) const {
		return ptr >= oth.ptr;
	}
	
	// ARITHMETIC OPERATORS
	simd_vector_iterator& operator++ () {
		++ptr;
		return *this;
	}
	simd_vector_iterator operator++ (int) {
		auto tmp = *this;
		operator++();
		return tmp;
	}
	simd_vector_iterator& operator-- () {
		--ptr;
		return *this;
	}
	simd_vector_iterator operator-- (int) {
		auto tmp = *this;
		operator--();
		return tmp;
	}
	simd_vector_iterator& operator+= (difference_type n) {
		ptr += n;
		return *this;
	}
	simd_vector_iterator& operator-= (difference_type n) {
		ptr -= n;
		return *this;
	}
	simd_vector_iterator operator+ (difference_type n) const {
		return simd_vector_iterator(ptr + n);
	}
	simd_vector_iterator operator- (difference_type n) const {
		return simd_vector_iterator(ptr - n);
	}
	difference_type operator- (const simd_vector_iterator& oth) const {
		return ptr - oth.ptr;
	}

	// SIMD_ITERATOR RELATED
	difference_type lower_offset() {
		return (reinterpret_cast<size_type>(ptr) % sizeof(S)) / sizeof(T);
	}
	difference_type upper_offset() {
		size_type s = reinterpret_cast<size_type>(ptr);
		difference_type d = (K - (s % sizeof(S) / sizeof(T))) % K;
		return -d;
	}
	S* lower_block() {
		return reinterpret_cast<S*>(ptr-lower_offset());
	}
	S* upper_block() {
		return reinterpret_cast<S*>(ptr-upper_offset());
	}
};

//ITERATOR inverse addition
template< typename T, typename S>
simd_vector_iterator< T, S> operator+( std::ptrdiff_t b, simd_vector_iterator< T, S> a ) {
	return a + b;
}

template< typename T, typename S>
class simd_vector {
private:
	T* original_alloc;
	T* arr;
	size_t size_;
	static const size_t K = sizeof(S)/sizeof(T);
public:
	typedef simd_vector_iterator<T, S> iterator;
	typedef S* simd_iterator;

public:
	explicit simd_vector(std::size_t s) {
		//USING ALIGNED MALLOC - works in Visual Studio 2013, not the same way in g++
		//auto addr = _aligned_malloc(sizeof(T)*s, sizeof(S));
		//arr = reinterpret_cast<T*>(addr);
		//HANDMADE VERSION - works in both Visual Studio 2013 and g++
		//not sure if aligned block <==> addres divisible by alignment, but works on 
		//regular systems with fairly small alignment
		original_alloc = new T[s + K];
		size_t addr = reinterpret_cast<size_t>(original_alloc);
		addr = (addr / sizeof(S)) * sizeof(S)+sizeof(S);
		arr = reinterpret_cast<T*>(addr);
		//USING STD::ALIGN - works in Visual Studio 2013, not in g++
		//void* tmp = reinterpret_cast<void*>(original_alloc);
		//size_t space = sizeof(T)*(s+K);
		//arr = reinterpret_cast<T*>( std::align(sizeof(S), sizeof(T)*s, tmp, space) );
		size_ = s;
	}

	//COPY
	simd_vector(const simd_vector& vec) =delete;
	simd_vector& operator= (const simd_vector& vec) =delete;
	//MOVE
	simd_vector(simd_vector&& vec) : 
		original_alloc (std::move(vec.original_alloc)),
		arr (std::move(vec.arr)), 
		size_ (vec.size_) {
		
		vec.original_alloc = nullptr;
		vec.arr = nullptr;
	}
	simd_vector& operator= (simd_vector&& vec) {
		delete[] original_alloc;
		original_alloc = vec.original_alloc;
		vec.original_alloc = nullptr;
		arr = vec.arr;
		vec.arr = nullptr;
		size_ = vec.size_;
		return *this;
	}
public:

	~simd_vector() {
		delete[] original_alloc;
	}

	iterator begin() {
		return iterator(arr);
	}
	iterator end() {
		return iterator(arr, size_);
	}

	std::size_t size() {
		return size_;
	}
};
